class StaticPagesController < ApplicationController
	skip_before_filter :require_login
  def home
  	if current_user
  		redirect_to user_path(current_user)
  	end
  end

  def help
  end

  def about
  end

  def contact
  end
end
