class UserGroupsController < ApplicationController
  before_action :set_user_group, only: [:show, :edit, :update, :destroy]
  before_action :if_not_part_of_group, only: [:show, :edit, :update, :destroy]
  before_action :not_group_owner, only: [:destroy]
  #load_and_authorize_resource

  # GET /user_groups
  # GET /user_groups.json
  def index
    @user_groups = UserGroup.paginate(page: params[:page])#.all
  end

  # GET /user_groups/1
  # GET /user_groups/1.json
  def show
    @token = params[:accept_token]
    @group_owner = @user_group.users.where(id: @user_group.user_id).first
  end

  # GET /user_groups/new
  def new
    @user_group = UserGroup.new
  end

  # GET /user_groups/1/edit
  def edit
    @invite = Invite.new
  end

  # POST /user_groups
  # POST /user_groups.json
  def create
    @user_group = UserGroup.new(user_group_params)
    respond_to do |format|
      if @user_group.save
        # create membership of owner of group
        group_owner = Membership.create(user_id: current_user.id, user_group_id: @user_group.id)
        group_owner.save
        format.html { redirect_to @user_group, notice: 'User group was successfully created.' }
        format.json { render :show, status: :created, location: @user_group }
      else
        format.html { render :new }
        format.json { render json: @user_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_groups/1
  # PATCH/PUT /user_groups/1.json
  def update
    respond_to do |format|
      if @user_group.update(user_group_params)
        format.html { redirect_to @user_group, notice: 'User group was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_group }
      else
        format.html { render :edit }
        format.json { render json: @user_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_groups/1
  # DELETE /user_groups/1.json
  def destroy
    @user_group.destroy
    respond_to do |format|
      format.html { redirect_to user_groups_url, notice: 'User group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_group
      @user_group = UserGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_group_params
      params.require(:user_group).permit(:name, :user_id)
    end

    def if_not_part_of_group
      @user_membership = current_user.memberships.where(user_id: current_user.id, user_group_id: @user_group.id).first

      if !@user_membership && !current_user.admin?
        redirect_to user_groups_path
        flash[:danger] = "You are not member of this group"
      end
    end

    def not_group_owner
      if current_user.id != @user_group.user_id && !current_user.admin?
        redirect_to user_groups_path
        flash[:danger] = "You are not permitted to do this"
      end
    end
end
