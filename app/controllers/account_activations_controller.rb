class AccountActivationsController < ApplicationController
	skip_before_filter :require_login
	def edit
		user = User.find_by(email: params[:email])
		if user && !user.activated? && user.authenticated?(:activation, params[:id])
			user.activate
			user.update_attribute( :activated, true )
			user.update_attribute( :activated_at, Time.zone.now )
			log_in user
			flash[:success] = "Account activaated"
			redirect_to user
		else
			flash[:error] = "Invalid activation link"
			redirect_to root_url
		end
	end
end
