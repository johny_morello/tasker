require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test 'should get links' do
  	get root_path
  	assert_template 'static_pages/home' #zapewnij ze jest wygenerowane static_pages/home
  	assert_select "a[href=?]", root_path, count: 2
  	assert_select "a[href=?]", help_path #zapewnij obecnosc linku w obiekcie html <a href="">
  	assert_select "a[href=?]", about_path #link /about jeswt wlozony w ? za pomoca about_path
  	assert_select "a[href=?]", contact_path
    get signup_path
    assert_select "title", full_title("Sign up")
  end
end
